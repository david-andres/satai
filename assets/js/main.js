var map;

var searchRad;
var searchCenter;

var updateTimestamp;

var twitterAPI_response;
var twtAPIcallCount = 0;

var predictionAPI_url = null;

var tweets = [];
var stats = {};

var sample_tweets;

var color_neg = '#ff0066';
var color_pos = '#0ce8fc';

var timeChart;
var timeChart_data = {
    negative: [0,0,0,0,0,0,0],
    positive: [0,0,0,0,0,0,0]
};


$(document).ready(function() {

    var searchCenter = L.latLng(38.346173, -0.490712);

    map = L.map('map').setView(searchCenter, 12);

    var tiles = L.tileLayer(
        'https://api.mapbox.com/styles/v1/david-andres/cjqof35pg6szj2smmcweryxyz/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZGF2aWQtYW5kcmVzIiwiYSI6IlR1VzBZdWcifQ.QdH98u2mZDWgYzgACJWjJw', {
        attribution: '© <a href="https://www.mapbox.com/map-feedback/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);


    searchRad = $('#search-rad-input').val();

    var map_overlay = L.featureGroup();

    searchRad_circle = L.circle(map.getCenter(),{
        radius: searchRad*1000,
        color: 'blue',
        weight: 3,
        opacity: 0.7,
        fillColor: '#404040',
        fillOpacity: 0.1
    }).addTo(map_overlay);
    map_overlay.addTo(map);


    map.on('click', function(e) {
        searchRad_circle.setLatLng(e.latlng);
        searchCenter = e.latlng;
    });

    /*
    var points = [];

    for (var key in sample_tweets) {
        if (tweets.hasOwnProperty(key)) {
            var val = tweets[key]['location'];
            if ( tweets[key]['location'] != '[,]') {
                points.push([
                    tweets[key]['location'][0] + Math.random()*0.05,
                    tweets[key]['location'][1] + Math.random()*0.05,
                    String(parseInt(Math.random()))
                ]);
            }
        }
    }

    var heat = L.heatLayer(points, {
        minOpacity: 0.7,
        radius: 40,
        blur: 30,
        gradient: {0.3: 'pink', 0.7: 'red', 1: 'blue'}
    }).addTo(map);
    */

    init_ui();


    $('#search-button').on('click', function()
    {
        reset_search();

        var query =  $('#search-query-input').val();

        if (query != '') {
            //console.log(searchCenter);
            get_twitterAPI_data(updateTimestamp, query, searchCenter);
        }
    });

    $('#search-rad-slider').on('input', function()
    {
        $('#search-rad-input').val( $(this).val() );

        $('#search-rad-input').trigger('change');
    });

    $('#search-rad-input').on('change', function()
    {
        reset_search();

        searchRad = $(this).val();

        searchRad_circle.setRadius(searchRad*1000);
    });

});


function reset_search()
{
    updateTimestamp = Date.now();

    tweets = [];

    reset_ui();

    console.log('Reset');
    console.log(tweets);
}


function get_twitterAPI_data(searchTimestamp, query, center, next_results=null)
{
    console.log('timestamp: ' + searchTimestamp);
    if (searchTimestamp != updateTimestamp) {
        reset_log();
    }

    var url = window.location.href + 'twitterapi/';
    if (next_results) {
        url += next_results;
        console.log(url);
    } else {
        url += '?';
        url += 'q=' + query + '&lat=' + center.lat + '&lng=' + center.lng + '&rad=' + searchRad + 'km';
    }

    $.ajax({
        url: url,
        method: 'POST',
        data: {
            'query': query,
            'lat': center.lat,
            'lng': center.lng,
            'searchRad': searchRad + 'km',
            'next_results': next_results
        },
        beforeSend: function() {
            console.log('sending');
            log_message('Conectando a Twitter API...');
        },
        success: function(response) {
            console.log('success');
            console.log(response);
            twitterAPI_response = JSON.parse(response);
            console.log(twitterAPI_response);
        },
        complete: function() {
            console.log('complete');

            twtAPIcallCount++;

            call_predictionAPI(twitterAPI_response);

            console.log(twitterAPI_response.search_metadata.next_results);
            if ( searchTimestamp == updateTimestamp && twitterAPI_response.search_metadata.next_results && twtAPIcallCount < 3) {
                setTimeout(function () {
                    get_twitterAPI_data(searchTimestamp, query, center, twitterAPI_response.search_metadata.next_results);
                }, 3000);
            } else {

            }
        },
        error: function(error){
            console.log(error);
        }
    });
}




function call_predictionAPI(twitterAPI_response)
{
    console.log('init sending to predictionAPI');
    log_message('Calculando polaridad...');

    var tweets_pack = [];
    var tweets_toAPI = { tweet: {}};

    for (var i = 0; i < twitterAPI_response.statuses.length; i++)
    {
        var obj = twitterAPI_response.statuses[i];

        tweets_pack[i] = {
            text: obj.full_text,
            location: obj.user.location,
            created_at: obj.created_at
        }

        tweets_toAPI.tweet[i] = obj.full_text;
    }

    var tweets_toAPI_json = JSON.stringify(tweets_toAPI);
    //console.log('tweets_toAPI: ' + tweets_toAPI_json);

    if (predictionAPI_url)
    {
        $.ajax({
            url: predictionAPI_url,
            method: 'POST',
            data: tweets_toAPI_json,
            dataType: 'json',
            beforeSend: function() {
                console.log('sending to prediction API');
            },
            success: function (response) {
                console.log('success');

                var tweets_fromAPI = JSON.parse(response);

                for (var i = 0; i < tweets_pack.length; i++) {
                    tweets_pack[i].polarity = tweets_fromAPI.tweet[i];
                }
            },
            complete: function(response) {
                console.log('complete');
                console.log(response);
            },
            error: function(error){
                console.log(error);
            }
        });
    }

    console.log(tweets_pack);
    tweets.push(tweets_pack);
    console.log(tweets);

    var total_tweets;

    for (var i = 0; i < tweets_pack.length; i++) {
        log_tweet(tweets_pack[i].text, tweets_pack[i].created_at, tweets_pack[i].location, tweets_pack[i].polarity);
    }

    update_ui();
}

function update_stats()
{
    total_tweets = 0;
    for (var i = 0; i < tweets.length; i++) {
        total_tweets += tweets[i].length;
    }

    log_message(total_tweets + ' tweets procesados.');
    stats.total_tweets = total_tweets;

    var total_pos = 0;
    var total_neg = 0;
    for (var i = 0; i < tweets.length; i++) {
        for (var j = 0; j < tweets[i].length; j++) {

            if ( typeof(tweets[i][j].polarity) === 'undefined' ) {
                if ( Math.random(0,1) < 0.5) {
                    total_neg++;
                } else {
                    total_pos++;
                }
            } else {
                if ( tweets[i][j].polarity == 0 ) {
                    total_neg++;
                } else {
                    total_pos++;
                }
            }

        }
    }
    stats.total_tweets_pos = total_pos;
    stats.total_tweets_neg = total_neg;

    console.log(stats);
}


function reset_stats()
{
    total_tweets = 0;
    stats.total_tweets_pos = 0;
    stats.total_tweets_neg = 0;
}


function log_reset()
{
    $('#log-message').empty();
    $('#log-tweets').empty();
}

function log_message(text)
{
    $('#log-message').empty();
    $('#log-message').prepend('<div class="log-message__item">' + text + '</div>');
}

function log_tweet(text, created_at, location, polarity)
{
    var polarity_class = null;
    if (polarity == 0) {
        polarity = 'polarity--negative';
    } else if (polarity == 1) {
        polarity = 'polarity--positive';
    }

    $('#log-tweets').prepend(
        '<div class="log-tweets__item log__item--tweet ' + polarity_class + '">' +
            '<div class="log-tweets__item__meta">' + moment(created_at, 'dd MMM DD HH:mm:ss ZZ YYYY').format('DD-MM-YYYY HH:mm:ss') + ' | ' + location + '</div>' +
            '<div class="log-tweets__item__text">' + text + '</div>' +
        '</div>'
    );
}



function init_ui()
{
    init_timeChart();

    init_graph();
}

function update_ui()
{
    update_stats();

    update_timeChart();

    update_graph();
}

function reset_ui()
{
    reset_stats();

    reset_timeChart();

    reset_graph();
}


function init_graph()
{
    $('#graph__side--neg').css({ 'background-color': color_neg });
    $('#graph__side--pos').css({ 'background-color': color_pos });
}

function update_graph()
{
    $('#graph__side--neg').animate({ width: (100*stats.total_tweets_neg/stats.total_tweets) + '%' }, 400);
    $('#graph__side--pos').animate({ width: (100*stats.total_tweets_pos/stats.total_tweets) + '%' }, 400);
}

function reset_graph()
{
    $('#graph__side--neg').animate({ width: 0 }, 100);
    $('#graph__side--pos').animate({ width: 0 }, 100);
}


function init_timeChart()
{
    var now = moment().add(1, 'days');

    var chartDays = [];
    for (var i=0; i < 7; i++)
    {
        chartDays[i] = moment().subtract(i, 'days');
    }
    chartDays.reverse();

    var chart_labels = [];
    for (var i=0; i < chartDays.length; i++) {
        chart_labels[i] = chartDays[i].format('DD/MM');
    }

    var chart_canvas = document.getElementById('timeChart-canvas').getContext('2d');
    var chart_config = {
        type: 'line',
        data: {
            labels: chart_labels,
            datasets: [{
                label: 'Negativos',
                fill: false,
                backgroundColor: color_neg,
                borderColor: color_neg,
                id: 'negative',
                data: timeChart_data.negative,
            },
            {
                label: 'Positivos',
                backgroundColor: color_pos,
                borderColor: color_pos,
                id: 'positive',
                data: timeChart_data.positive,
                fill: false,
            }]
        },
        options: {
            responsive: true,
            title: {
                display: false,
                text: 'Tweets'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Dia'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Numero de tweets'
                    }
                }]
            }
        }
    };

    timeChart = new Chart(chart_canvas, chart_config);
}


function reset_timeChart()
{
    timeChart.data.datasets.forEach((dataset) => {
        dataset.data = [];
    });
    timeChart.update();
}


function update_timeChart()
{
    var now = moment();

    var tweetDays = [];
    for (var i=0; i < 8; i++)
    {
        tweetDays[i] = moment().subtract(i, 'days');
    }
    tweetDays.reverse();

    for (var i=0; i < tweets.length; i++)
    {
        for (var j=0; j < tweets[i].length; j++)
        {
            var tweetDate = moment(tweets[i][j].created_at, 'dd MMM DD HH:mm:ss ZZ YYYY', 'en');

            for (var d=0; d < 7; d++)
            {
                if ( tweetDate.isBetween(tweetDays[d], tweetDays[d+1] ) )
                {
                    if ( typeof(tweets[i][j].polarity) === 'undefined' ) {
                        if ( Math.random(0,1) < 0.6) {
                            timeChart_data.negative[d] += 1;
                        } else {
                            timeChart_data.positive[d] += 1;
                        }
                    } else {
                        if ( tweets[i][j].polarity == 0 ) {
                            timeChart_data.negative[d] += 1;
                        } else {
                            timeChart_data.positive[d] += 1;
                        }
                    }
                }
            }
        }

    }

    timeChart.data.datasets.forEach((dataset) => {
        if (dataset.id == 'negative') {
            dataset.data = timeChart_data.negative;
        } else if (dataset.id == 'positive') {
            dataset.data = timeChart_data.positive;
        }
    });
    timeChart.update();
}
